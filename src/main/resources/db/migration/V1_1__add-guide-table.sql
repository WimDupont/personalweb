CREATE TABLE IF NOT EXISTS guide (
    path VARCHAR(200) primary key NOT NULL,
    content_sha256 VARCHAR(256) UNIQUE NOT NULL,
    html_text TEXT,
    created_date DATETIME,
    modified_date DATETIME
)
