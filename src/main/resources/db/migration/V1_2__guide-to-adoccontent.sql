DELETE FROM guide;

RENAME TABLE guide TO adoc_content;

ALTER TABLE adoc_content ADD COLUMN committed_date DATETIME;
ALTER TABLE adoc_content ADD COLUMN content_type VARCHAR(20) NOT NULL;

ALTER TABLE adoc_content ADD INDEX content_type_idx (content_type);
