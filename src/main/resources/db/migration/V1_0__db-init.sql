CREATE TABLE IF NOT EXISTS book (
    id VARCHAR(36) primary key NOT NULL,
    title VARCHAR(200) NOT NULL,
    author VARCHAR(500) NOT NULL,
    isbn VARCHAR(100) UNIQUE NOT NULL,
    category VARCHAR(100) NOT NULL,
    series VARCHAR(200),
    series_number DECIMAL(2,1)
)
