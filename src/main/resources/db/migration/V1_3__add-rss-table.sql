CREATE TABLE IF NOT EXISTS rss (
    id VARCHAR(36) primary key NOT NULL,
    content TEXT,
    created_date DATETIME,
    modified_date DATETIME
)
