Use any amount of requests free of charge.

Most of the endpoints offer functionality already provided by your OS, but I'd like to expose them anyway since people often navigate to their browser for these. 

*[Donations are accepted](https://wimdupont.com/donate)*
