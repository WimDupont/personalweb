package com.wimdupont.personalweb.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/links")
public class LinkController {

    @GetMapping
    public String getRss(Model model) {
        return "links";
    }

}
