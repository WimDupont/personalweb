package com.wimdupont.personalweb.controller.web;

import com.wimdupont.personalweb.api.AffirmationApi;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    private final AffirmationApi affirmationApi;

    public IndexController(AffirmationApi affirmationApi) {
        this.affirmationApi = affirmationApi;
    }

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("affirmation", affirmationApi.getAffirmation());
        return "home";
    }

}
