package com.wimdupont.personalweb.controller.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static com.wimdupont.personalweb.util.Constants.GPG_PUBLIC_KEY;

@Controller
@RequestMapping("/contact")
public class ContactController {

    private static final Logger LOG = LoggerFactory.getLogger(ContactController.class);

    @GetMapping
    public String getContact(Model model) {
        return "contact";
    }

    @GetMapping(path = "/pubkey.gpg")
    public ResponseEntity<Resource> getPublicKey(Model model) {
        try {
            File file = new File(GPG_PUBLIC_KEY);
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            return ResponseEntity.ok()
                    .contentLength(file.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }
}
