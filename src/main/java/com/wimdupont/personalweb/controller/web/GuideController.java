package com.wimdupont.personalweb.controller.web;

import com.wimdupont.personalweb.converter.AdocContentMetaToDtoConverter;
import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.service.AdocContentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/guides")
public class GuideController {

    private final AdocContentService adocContentService;
    private final AdocContentMetaToDtoConverter adocContentMetaToDtoConverter;

    public GuideController(AdocContentService adocContentService,
                           AdocContentMetaToDtoConverter adocContentMetaToDtoConverter) {
        this.adocContentService = adocContentService;
        this.adocContentMetaToDtoConverter = adocContentMetaToDtoConverter;
    }

    @GetMapping
    public String getGuides(Model model) {
        model.addAttribute("guides", adocContentService
                .findAllMetaData(AdocContentType.GUIDE).stream()
                .map(adocContentMetaToDtoConverter::convertForMetaData)
                .toList());
        return "guides";
    }

    @GetMapping(value = "/{name}")
    public String getGuide(@PathVariable(value = "name") String name, Model model) {
        var text = adocContentService
                .findByByTitle(name, AdocContentType.GUIDE)
                .orElseThrow()
                .getHtmlText();
        model.addAttribute("guide", text != null
                ? text
                : "Oops, nothing here.");
        model.addAttribute("title", name);
        return "guide";
    }
}
