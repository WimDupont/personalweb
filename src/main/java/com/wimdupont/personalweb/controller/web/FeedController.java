package com.wimdupont.personalweb.controller.web;

import com.wimdupont.personalweb.service.RssService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@Controller
public class FeedController {

    private final RssService rssService;

    public FeedController(RssService rssService) {
        this.rssService = rssService;
    }

    @GetMapping(path = "/rss.xml")
    @ResponseBody
    public ResponseEntity<String> getRss() {
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(APPLICATION_XML_VALUE))
                .body(rssService.retrieveRss().getContent());
    }
}
