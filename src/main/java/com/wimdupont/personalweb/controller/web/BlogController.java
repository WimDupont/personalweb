package com.wimdupont.personalweb.controller.web;

import com.wimdupont.personalweb.converter.AdocContentMetaToDtoConverter;
import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.service.AdocContentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/blog")
public class BlogController {

    private final AdocContentService adocContentService;
    private final AdocContentMetaToDtoConverter adocContentMetaToDtoConverter;

    public BlogController(AdocContentService adocContentService,
                          AdocContentMetaToDtoConverter adocContentMetaToDtoConverter) {
        this.adocContentService = adocContentService;
        this.adocContentMetaToDtoConverter = adocContentMetaToDtoConverter;
    }

    @GetMapping
    public String getBlog(Model model) {
        model.addAttribute("articles", adocContentService
                .findAllMetaData(AdocContentType.BLOG_ARTICLE).stream()
                .map(adocContentMetaToDtoConverter::convertForMetaData)
                .toList());
        return "blog";
    }

    @GetMapping(value = "/article/{name}")
    public String getBlogArticle(@PathVariable(value = "name") String name, Model model) {
        var text = adocContentService
                .findByByTitle(name, AdocContentType.BLOG_ARTICLE)
                .orElseThrow()
                .getHtmlText();
        model.addAttribute("article", text != null
                ? text
                : "Oops, nothing here.");
        model.addAttribute("title", name);
        return "article";
    }
}
