package com.wimdupont.personalweb.controller.api;

import com.wimdupont.personalweb.model.Hash;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.xml.bind.DatatypeConverter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/api/cryptography")
@Tag(name = "Cryptography")
public class CryptographyController {

    @PostMapping(value = "/hash")
    @Operation(summary = "Hash value using the algorithm provided")
    public String hash(@RequestBody String value,
                       @RequestParam Hash algorithm) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(algorithm.getValue());
        byte[] hash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
        return DatatypeConverter.printHexBinary(hash);
    }
}
