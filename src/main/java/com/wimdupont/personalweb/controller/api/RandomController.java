package com.wimdupont.personalweb.controller.api;

import com.wimdupont.personalweb.model.Coin;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotEmpty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@RestController
@RequestMapping("/api/random")
@Tag(name = "Random", description = "Random generators")
public class RandomController {

    @GetMapping(value = "/uuid")
    @Operation(summary = "Generate random UUID")
    public String randomUuid() {
        return UUID.randomUUID().toString();
    }

    @GetMapping(value = "/coin-flip")
    @Operation(summary = "Flip a coin")
    public String coinFlip() {
        return Math.random() < 0.5
                ? Coin.HEADS.name()
                : Coin.TAILS.name();
    }

    @GetMapping(value = "/number")
    @Operation(summary = "Get random number")
    public int randomNumber(
            @RequestParam(defaultValue = "1") Integer min,
            @RequestParam(defaultValue = "6") Integer max) {
        return new Random().nextInt(max - min + 1) + min;
    }

    @PostMapping(value = "/name")
    @Operation(summary = "Get random value of list")
    public String name(@RequestBody @NotEmpty List<String> names) {
        return names.get(new Random().nextInt(names.size()));
    }
}
