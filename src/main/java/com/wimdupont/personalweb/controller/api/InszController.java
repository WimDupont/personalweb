package com.wimdupont.personalweb.controller.api;

import com.wimdupont.personalweb.model.Gender;
import com.wimdupont.personalweb.model.InszModel;
import com.wimdupont.personalweb.service.InszService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/insz")
@Tag(name = "INSZ", description = "Belgian national identification number")
public class InszController {

    private final InszService inszService;

    public InszController(InszService inszService) {
        this.inszService = inszService;
    }

    @GetMapping(value = "/generate")
    @Operation(summary = "Generate INSZ",
            description = "Generate random INSZ taking filled in parameters in account")
    public String generate(
            @RequestParam(required = false)
            @Parameter(description = "yyyy-MM-dd")
            LocalDate birthDate,
            @RequestParam(required = false)
            Integer birthCounter,
            @RequestParam(required = false)
            Gender gender) {
        return inszService.generate(new InszModel(birthDate, birthCounter, gender));
    }


    @GetMapping(value = "/valid")
    @Operation(summary = "Validate INSZ")
    public boolean validate(@RequestParam
                            @Parameter(description = "'.' and '-' characters are automatically removed.")
                            String insz,
                            @RequestParam(required = false)
                            @Parameter(description = "Both are considered when empty.")
                            Boolean bornAtOrAfter2000,
                            @RequestParam(required = false)
                            @Parameter(description = "Both are considered when empty.")
                            Gender gender) {
        return inszService.validate(insz, bornAtOrAfter2000, gender);
    }
}
