package com.wimdupont.personalweb.controller.api;

import com.wimdupont.personalweb.exception.Base64DecodingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@RestController
@RequestMapping("/api/encoding")
@Tag(name = "Encoding")
public class EncodingController {

    @PostMapping(value = "/base64/encode")
    @Operation(summary = "Encode value to base64")
    public String encodeBase64(@RequestBody String value) {
        return Base64.getEncoder().encodeToString(value.getBytes(StandardCharsets.UTF_8));
    }

    @PostMapping(value = "/base64/decode")
    @Operation(summary = "Decode value from base64")
    public String decodeBase64(@RequestBody String value) {
        try {
            return new String(Base64.getDecoder().decode(value), StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new Base64DecodingException();
        }
    }

}
