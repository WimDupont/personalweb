package com.wimdupont.personalweb.config;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AsciidoctorConfig {

    @Bean
    public Asciidoctor asciidoctor() {
        return Asciidoctor.Factory.create();
    }

    @Bean
    public Options options() {
        return Options.builder()
                .safe(SafeMode.UNSAFE)
                .build();
    }
}
