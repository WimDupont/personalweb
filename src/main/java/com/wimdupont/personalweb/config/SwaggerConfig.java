package com.wimdupont.personalweb.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Configuration
public class SwaggerConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(SwaggerConfig.class);
    private static final String DOCUMENTATION_FILE = "api-documentation.md";
    private final String serverUrl;

    public SwaggerConfig(@Value("${server.url}") String serverUrl) {
        this.serverUrl = serverUrl;
    }

    @Bean
    public OpenAPI getOpenAPI() {
        return new OpenAPI()
                .servers(StringUtils.isNotBlank(serverUrl)
                        ? List.of(new Server().url(serverUrl))
                        : null)
                .info(getInfo());
    }


    private Info getInfo() {
        Info info = new Info().title("Wim Dupont's API")
                .version("v1");

        try (InputStream in = getClass().getClassLoader().getResourceAsStream(DOCUMENTATION_FILE)) {
            if (in != null) {
                info.description(new String(in.readAllBytes()));
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return info;
    }

    @Bean
    public GroupedOpenApi overviewGroupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("overview")
                .pathsToMatch("/api/**")
                .build();
    }

    @Bean
    public String serverUrl() {
        return serverUrl;
    }
}
