package com.wimdupont.personalweb.repository;

import com.wimdupont.personalweb.model.dao.Rss;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RssRepository extends JpaRepository<Rss, String> {
}
