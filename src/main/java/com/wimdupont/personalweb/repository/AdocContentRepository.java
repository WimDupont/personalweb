package com.wimdupont.personalweb.repository;

import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.model.dao.AdocContent;
import com.wimdupont.personalweb.model.dao.projection.AdocContentMeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdocContentRepository extends JpaRepository<AdocContent, String> {

    Optional<AdocContent> findByPath(String path);

    Optional<AdocContent> findByContentSha256(String contentSha256);

    List<AdocContent> findAllByContentType(AdocContentType contentType);

    List<AdocContent> findAllByContentTypeOrderByCommittedDateDesc(AdocContentType contentType);

    List<AdocContentMeta> findAllMetaDataByContentTypeOrderByCommittedDateDesc(AdocContentType contentType);
}
