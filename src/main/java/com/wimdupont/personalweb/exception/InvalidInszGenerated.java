package com.wimdupont.personalweb.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InvalidInszGenerated extends RuntimeException {

    public InvalidInszGenerated(String insz) {
        super(String.format("Generated invalid INSZ: %s", insz));
    }
}
