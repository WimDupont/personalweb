package com.wimdupont.personalweb.exception;

public class RssXmlMapperException extends RuntimeException{

    public RssXmlMapperException(Throwable throwable){
        super(throwable);
    }
}
