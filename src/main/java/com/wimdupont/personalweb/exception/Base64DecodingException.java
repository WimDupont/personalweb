package com.wimdupont.personalweb.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class Base64DecodingException extends RuntimeException {

    public Base64DecodingException() {
        super("Unable to decode given value. Please make sure it is base64 encoded.");
    }

}
