package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.converter.BookToDtoConverter;
import com.wimdupont.personalweb.model.dto.BookDto;
import com.wimdupont.personalweb.repository.BookRepository;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class BookService {

    private final BookRepository bookRepository;
    private final BookToDtoConverter bookToDtoConverter;

    public BookService(BookRepository bookRepository,
                       BookToDtoConverter bookToDtoConverter) {
        this.bookRepository = bookRepository;
        this.bookToDtoConverter = bookToDtoConverter;
    }

    public Map<String, List<BookDto>> findAllSortedByCategory() {
        return bookRepository.findAll(Sort.by("category", "author", "series", "seriesNumber", "title").ascending())
                .stream()
                .map(bookToDtoConverter::convert)
                .collect(Collectors.groupingBy(BookDto::category,
                        LinkedHashMap::new,
                        Collectors.mapping(book -> book, Collectors.toList())));
    }
}
