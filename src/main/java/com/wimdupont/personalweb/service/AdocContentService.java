package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.api.dto.RepositoryFile;
import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.model.dao.AdocContent;
import com.wimdupont.personalweb.model.dao.projection.AdocContentMeta;
import com.wimdupont.personalweb.repository.AdocContentRepository;
import com.wimdupont.personalweb.util.AdocContentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AdocContentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdocContentService.class);
    private final AdocContentRepository adocContentRepository;

    public AdocContentService(AdocContentRepository adocContentRepository) {
        this.adocContentRepository = adocContentRepository;
    }

    public List<AdocContent> findAllRSSData() {
        return adocContentRepository.findAllByContentTypeOrderByCommittedDateDesc(AdocContentType.BLOG_ARTICLE);
    }

    public List<AdocContentMeta> findAllMetaData(AdocContentType adocContentType) {
        return adocContentRepository.findAllMetaDataByContentTypeOrderByCommittedDateDesc(adocContentType);
    }

    public Optional<AdocContent> findByByTitle(String title, AdocContentType adocContentType) {
        return adocContentRepository.findByPath(AdocContentUtil.toPath(title, adocContentType));
    }

    public void removeWhenRemoteNotFound(AdocContentType adocContentType, List<RepositoryFile> repositoryFiles) {
        adocContentRepository.findAllByContentType(adocContentType).stream()
                .filter(adocContent -> !repositoryFiles.stream()
                        .map(RepositoryFile::path)
                        .toList().contains(adocContent.getPath()))
                .peek(content -> LOGGER.info("Removing adoc content: {}", content.getPath()))
                .forEach(adocContentRepository::delete);
    }

    public void save(AdocContent adocContent) {
        var saved = adocContentRepository.save(adocContent);
        LOGGER.info("{} saved: {}", saved.getContentType().name(), saved.getPath());
    }

    public Optional<AdocContent> findByContentSha256(String contentSha256) {
        return adocContentRepository.findByContentSha256(contentSha256);
    }

}
