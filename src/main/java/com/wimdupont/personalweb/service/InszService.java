package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.exception.InvalidInszGenerated;
import com.wimdupont.personalweb.model.Gender;
import com.wimdupont.personalweb.model.InszModel;
import jakarta.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class InszService {

    public String generate(@Valid InszModel insz) {
        var year = StringUtils.leftPad(String.valueOf(insz.birthDate().getYear()), 4, "0").substring(2);
        var month = StringUtils.leftPad(String.valueOf(insz.birthDate().getMonthValue()), 2, "0");
        var day = StringUtils.leftPad(String.valueOf(insz.birthDate().getDayOfMonth()), 2, "0");
        var birthCounterParam = insz.birthCounter();

        if (hasNoValidBirthCounter(String.valueOf(insz.birthCounter()), insz.gender())) {
            if (insz.birthCounter() == 998) {
                --birthCounterParam;
            } else {
                ++birthCounterParam;
            }
        }
        var birthCounter = StringUtils.leftPad(String.valueOf(birthCounterParam), 3, "0");

        var baseNumber = year + month + day + birthCounter;
        var bornAtOrAfter2000 = insz.birthDate().getYear() >= 2000;

        var result = baseNumber + getChecksum(baseNumber, bornAtOrAfter2000);

        if (!validate(result, bornAtOrAfter2000, insz.gender())) {
            throw new InvalidInszGenerated(result);
        }

        return result;
    }

    private String getChecksum(String baseNumber, boolean bornAtOrAfter2000) {
        if (bornAtOrAfter2000) {
            baseNumber = 2 + baseNumber;
        }
        var checkSum = 97 - Long.parseLong(baseNumber) % 97;
        return StringUtils.leftPad(String.valueOf(checkSum), 2, "0");
    }


    public boolean validate(String insz, Boolean bornAtOrAfter2000, Gender gender) {
        if (insz == null) {
            return false;
        }
        insz = insz.replaceAll("[.-]", "");

        if (insz.length() != 11) {
            return false;
        }

        if (hasNoValidBirthCounter(insz.substring(6, 9), gender)) {
            return false;
        }

        var baseNumber = insz.substring(0, 9);
        var checksum = insz.substring(9, 11);

        if (bornAtOrAfter2000 == null) {
            return hasCorrectChecksum(baseNumber, checksum) || hasCorrectChecksum(2 + baseNumber, checksum);
        } else {
            return bornAtOrAfter2000
                    ? hasCorrectChecksum(2 + baseNumber, checksum)
                    : hasCorrectChecksum(baseNumber, checksum);
        }
    }

    private boolean hasCorrectChecksum(String baseNumber, String checkSum) throws NumberFormatException {
        try {
            return 97 - Long.parseLong(baseNumber) % 97 == Integer.parseInt(checkSum);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean hasNoValidBirthCounter(String birthCounter, Gender gender) {
        if (gender == null) {
            return false;
        }
        try {
            return !switch (gender) {
                case MALE -> Integer.parseInt(birthCounter) % 2 != 0;
                case FEMALE -> Integer.parseInt(birthCounter) % 2 == 0;
            };
        } catch (NumberFormatException e) {
            return true;
        }
    }
}
