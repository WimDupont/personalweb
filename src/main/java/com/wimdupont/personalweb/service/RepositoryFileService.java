package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.api.GitApi;
import com.wimdupont.personalweb.api.dto.Commit;
import com.wimdupont.personalweb.api.dto.RepositoryFile;
import com.wimdupont.personalweb.model.AdocContentType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RepositoryFileService {

    private final GitApi gitApi;

    public RepositoryFileService(GitApi gitApi) {
        this.gitApi = gitApi;
    }

    public List<RepositoryFile> findRepositoryFiles(AdocContentType adocContentType) {
        var repositoryFiles = new ArrayList<RepositoryFile>();
        gitApi.getRepositoryTree(adocContentType)
                .orElseThrow().stream()
                .map(repositoryTreeItem -> gitApi.getRepositoryFile(repositoryTreeItem.path()))
                .forEach(repositoryFile -> repositoryFile.ifPresent(repositoryFiles::add));
        return repositoryFiles;
    }

    public Optional<Commit> getCommit(String commitId){
        return gitApi.getCommit(commitId);
    }
}
