package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.model.rss.Channel;
import com.wimdupont.personalweb.model.rss.Item;
import com.wimdupont.personalweb.model.rss.RssFeed;
import com.wimdupont.personalweb.util.AdocContentUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RssFeedGenerator {

    private final AdocContentService adocContentService;
    private final RssService rssService;

    public RssFeedGenerator(AdocContentService adocContentService,
                            RssService rssService) {
        this.adocContentService = adocContentService;
        this.rssService = rssService;
    }

    public void generate() {
        Channel channel = Channel.Builder.newBuilder()
                .title("Wim Dupont")
                .pubDate(LocalDateTime.now().toString())
                .link("https://wimdupont.com")
                .description("Updates from Wim Dupont")
                .language("en-US")
                .item(getItems())
                .build();

        rssService.save(RssFeed.Builder.newBuilder()
                .version("2.0")
                .channel(channel)
                .build());
    }

    private List<Item> getItems() {
        return adocContentService.findAllRSSData().stream()
                .map(adocContentRss -> buildItem(
                        adocContentRss.getCommittedDate(),
                        AdocContentUtil.toTitle(adocContentRss),
                        adocContentRss.getHtmlText()))
                .toList();
    }

    private Item buildItem(LocalDateTime postDate, String title, String content) {
        String url = String.format("https://wimdupont.com/blog/article/%s", title);
        return Item.Builder.newBuilder()
                .title(title)
                .link(url)
                .pubDate(postDate.toString())
                .guid(title + "-" + postDate)
                .author("Wim Dupont")
                .description(content)
                .build();
    }
}
