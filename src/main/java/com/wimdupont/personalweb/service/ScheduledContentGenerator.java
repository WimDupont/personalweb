package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.converter.AdocToHtmlStringConverter;
import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.model.dao.AdocContent;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;

@Service
@Transactional
public class ScheduledContentGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledContentGenerator.class);
    private final RssFeedGenerator rssFeedGenerator;
    private final AdocContentService adocContentService;
    private final RepositoryFileService repositoryFileService;
    private final AdocToHtmlStringConverter adocToHtmlStringConverter;

    public ScheduledContentGenerator(RssFeedGenerator rssFeedGenerator,
                                     AdocContentService adocContentService,
                                     RepositoryFileService repositoryFileService,
                                     AdocToHtmlStringConverter adocToHtmlStringConverter) {
        this.rssFeedGenerator = rssFeedGenerator;
        this.adocContentService = adocContentService;
        this.repositoryFileService = repositoryFileService;
        this.adocToHtmlStringConverter = adocToHtmlStringConverter;
    }

    @Scheduled(cron = "${scheduled.content-generator.cron}")
    public void generate() {
        LOGGER.info("Content generator started.");
        updateAdocContent();
        rssFeedGenerator.generate();
        LOGGER.info("Content generator completed.");
    }

    private void updateAdocContent() {
        for (AdocContentType contentType : AdocContentType.values()) {
            var remoteRepositoryFiles = repositoryFileService.findRepositoryFiles(contentType);
            adocContentService.removeWhenRemoteNotFound(contentType, remoteRepositoryFiles);
            remoteRepositoryFiles.stream()
                    .filter(repositoryFile -> adocContentService
                            .findByContentSha256(repositoryFile.contentSha256())
                            .isEmpty())
                    .map(repositoryFile -> AdocContent.Builder.newBuilder()
                            .path(repositoryFile.path())
                            .contentSha256(repositoryFile.contentSha256())
                            .htmlText(toHtml(repositoryFile.contentBase64()))
                            .contentType(contentType)
                            .committedDate(fetchCommittedDate(repositoryFile.lastCommitId()))
                            .build())
                    .forEach(adocContentService::save);
        }
    }

    private String toHtml(String contentBase64) {
        return adocToHtmlStringConverter.convert(new String(Base64.getDecoder()
                .decode(contentBase64.getBytes(StandardCharsets.UTF_8))));
    }

    private LocalDateTime fetchCommittedDate(String commitId) {
        return LocalDateTime.ofInstant(repositoryFileService
                .getCommit(commitId)
                .orElseThrow()
                .committedDate(), ZoneId.systemDefault());
    }
}
