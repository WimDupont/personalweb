package com.wimdupont.personalweb.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.wimdupont.personalweb.exception.RssXmlMapperException;
import com.wimdupont.personalweb.model.dao.Rss;
import com.wimdupont.personalweb.model.rss.RssFeed;
import com.wimdupont.personalweb.repository.RssRepository;
import org.springframework.stereotype.Service;

@Service
public class RssService {

    private final XmlMapper xmlMapper;
    private final RssRepository rssRepository;

    public RssService(XmlMapper xmlMapper,
                      RssRepository rssRepository) {
        this.xmlMapper = xmlMapper;
        this.rssRepository = rssRepository;
    }

    public Rss retrieveRss() {
        return rssRepository.findAll().stream()
                .findAny()
                .orElse(new Rss());
    }

    public void save(RssFeed rssFeed) {
        try {
            var rss = retrieveRss();
            rss.setContent(xmlMapper.writeValueAsString(rssFeed));
            rssRepository.save(rss);
        } catch (JsonProcessingException e) {
            throw new RssXmlMapperException(e);
        }
    }
}
