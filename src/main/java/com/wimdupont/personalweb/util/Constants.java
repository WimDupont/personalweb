package com.wimdupont.personalweb.util;

public class Constants {

    private Constants() {
    }

    public static final String GPG_PUBLIC_KEY = System.getProperty("user.home") + "/personalweb/gpg/pub.asc";
    public static final String ADOC_SUFFIX = ".adoc";


}
