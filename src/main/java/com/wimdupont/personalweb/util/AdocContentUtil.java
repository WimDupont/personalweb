package com.wimdupont.personalweb.util;

import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.model.dao.projection.AdocContentMeta;

public class AdocContentUtil {

    private AdocContentUtil() {
    }

    public static String toPath(String title, AdocContentType adocContentType) {
        return adocContentType.getPathPrefix() + "/" + title + Constants.ADOC_SUFFIX;
    }

    public static String toTitle(AdocContentMeta adocContentMeta) {
        return adocContentMeta.getPath()
                .replace(Constants.ADOC_SUFFIX, "")
                .replaceFirst(adocContentMeta.getContentType().getPathPrefix() + "/", "");
    }

}
