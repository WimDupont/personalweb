package com.wimdupont.personalweb;

import com.wimdupont.personalweb.service.ScheduledContentGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableJpaAuditing
public class PersonalWebApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(PersonalWebApplication.class, args);
        context.getBean(ScheduledContentGenerator.class).generate();
    }
}
