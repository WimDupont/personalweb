package com.wimdupont.personalweb.model;

public enum AdocContentType {

    GUIDE("guides"),
    BLOG_ARTICLE("blog");

    private final String pathPrefix;

    public String getPathPrefix() {
        return pathPrefix;
    }

    AdocContentType(String pathPrefix) {
        this.pathPrefix = pathPrefix;
    }
}
