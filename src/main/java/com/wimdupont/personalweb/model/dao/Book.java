package com.wimdupont.personalweb.model.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;

import java.math.BigDecimal;


@Entity
public class Book {

    @Id
    @UuidGenerator
    private String id;

    private String title;
    private String author;
    private String isbn;
    private String category;
    private String series;
    private BigDecimal seriesNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public BigDecimal getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(BigDecimal seriesNumber) {
        this.seriesNumber = seriesNumber;
    }
}
