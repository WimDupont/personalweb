package com.wimdupont.personalweb.model.dao;

import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.model.dao.projection.AdocContentMeta;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;

import java.time.LocalDateTime;

@Entity
public class AdocContent extends Auditable implements AdocContentMeta {

    @Id
    private String path;
    private String contentSha256;
    private String htmlText;
    @Enumerated(EnumType.STRING)
    private AdocContentType contentType;
    private LocalDateTime committedDate;

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContentSha256() {
        return contentSha256;
    }

    public void setContentSha256(String contentSha256) {
        this.contentSha256 = contentSha256;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    @Override
    public AdocContentType getContentType() {
        return contentType;
    }

    public void setContentType(AdocContentType contentType) {
        this.contentType = contentType;
    }

    @Override
    public LocalDateTime getCommittedDate() {
        return committedDate;
    }

    public void setCommittedDate(LocalDateTime committedDate) {
        this.committedDate = committedDate;
    }

    protected AdocContent() {
    }

    private AdocContent(Builder builder) {
        path = builder.path;
        contentSha256 = builder.contentSha256;
        htmlText = builder.htmlText;
        contentType = builder.contentType;
        committedDate = builder.committedDate;
    }

    public static final class Builder {
        private String path;
        private String contentSha256;
        private String htmlText;
        private AdocContentType contentType;
        private LocalDateTime committedDate;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder path(String val) {
            path = val;
            return this;
        }

        public Builder contentSha256(String val) {
            contentSha256 = val;
            return this;
        }

        public Builder htmlText(String val) {
            htmlText = val;
            return this;
        }

        public Builder contentType(AdocContentType val) {
            contentType = val;
            return this;
        }

        public Builder committedDate(LocalDateTime val) {
            committedDate = val;
            return this;
        }

        public AdocContent build() {
            return new AdocContent(this);
        }
    }
}
