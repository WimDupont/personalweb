package com.wimdupont.personalweb.model.dao.projection;

import com.wimdupont.personalweb.model.AdocContentType;

import java.time.LocalDateTime;

public interface AdocContentMeta {

    String getPath();
    AdocContentType getContentType();
    LocalDateTime getCommittedDate();

}
