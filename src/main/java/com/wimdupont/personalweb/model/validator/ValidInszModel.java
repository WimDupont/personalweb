package com.wimdupont.personalweb.model.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = InszModelValidator.class)
@Target( { ElementType.TYPE, ElementType.RECORD_COMPONENT })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidInszModel {
    String message() default "Invalid INSZ number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
