package com.wimdupont.personalweb.model.validator;

import com.wimdupont.personalweb.model.InszModel;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;

public class InszModelValidator implements ConstraintValidator<ValidInszModel, InszModel> {

    @Override
    public boolean isValid(InszModel value, ConstraintValidatorContext context) {
        var minDate = LocalDate.of(0, 1, 1);
        var maxDate = LocalDate.of(2999, 12, 31);
        if (value.birthDate().isBefore(minDate) || value.birthDate().isAfter(maxDate)) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(
                            String.format("Please enter a birth date between %s and %s", minDate, maxDate))
                    .addConstraintViolation();
            return false;
        }
        return true;
    }
}
