package com.wimdupont.personalweb.model.dto;

import java.time.LocalDate;

public record AdocContentMetaDto(
        String title,
        LocalDate date
) {

    private AdocContentMetaDto(Builder builder) {
        this(builder.title,
                builder.date);
    }

    public static final class Builder {
        private String title;
        private LocalDate date;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder dateTime(LocalDate val) {
            date = val;
            return this;
        }

        public AdocContentMetaDto build() {
            return new AdocContentMetaDto(this);
        }
    }
}
