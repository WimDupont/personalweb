package com.wimdupont.personalweb.model.dto;

import java.math.BigDecimal;

public record BookDto(

        String id,
        String title,
        String author,
        String isbn,
        String category,
        String series,
        BigDecimal seriesNumber
) {

    public String seriesWithNumber() {
        if (series != null) {
            return seriesNumber != null
                    ? String.format("(%s #%s)", series, seriesNumber.stripTrailingZeros())
                    : String.format("(%s)", series);
        }
        return null;
    }

    private BookDto(Builder builder) {
        this(builder.id,
                builder.title,
                builder.author,
                builder.isbn,
                builder.category,
                builder.series,
                builder.seriesNumber);
    }

    public static final class Builder {
        private String id;
        private String title;
        private String author;
        private String isbn;
        private String category;
        private String series;
        private BigDecimal seriesNumber;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder author(String val) {
            author = val;
            return this;
        }

        public Builder isbn(String val) {
            isbn = val;
            return this;
        }

        public Builder category(String val) {
            category = val;
            return this;
        }

        public Builder series(String val) {
            series = val;
            return this;
        }

        public Builder seriesNumber(BigDecimal val) {
            seriesNumber = val;
            return this;
        }

        public BookDto build() {
            return new BookDto(this);
        }
    }
}
