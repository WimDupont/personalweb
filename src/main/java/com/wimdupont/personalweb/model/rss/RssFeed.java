package com.wimdupont.personalweb.model.rss;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "rss")
public record RssFeed(
        @JacksonXmlProperty(localName = "channel")
        Channel channel,

        @JacksonXmlProperty(isAttribute = true)
        String version
) {

    private RssFeed(Builder builder) {
        this(builder.channel,
                builder.version);
    }

    public static final class Builder {
        private @JacksonXmlProperty(localName = "channel") Channel channel;
        private @JacksonXmlProperty(isAttribute = true) String version;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder channel(@JacksonXmlProperty(localName = "channel") Channel val) {
            channel = val;
            return this;
        }

        public Builder version(@JacksonXmlProperty(isAttribute = true) String val) {
            version = val;
            return this;
        }

        public RssFeed build() {
            return new RssFeed(this);
        }
    }
}
