package com.wimdupont.personalweb.model.rss;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "item")
public record Item(
        @JacksonXmlProperty
        String title,
        @JacksonXmlProperty
        String description,
        @JacksonXmlProperty
        String link,
        @JacksonXmlProperty
        String author,
        @JacksonXmlProperty
        String pubDate,
        @JacksonXmlProperty
        String guid
) {


    private Item(Builder builder) {
        this(builder.title,
                builder.description,
                builder.link,
                builder.author,
                builder.pubDate,
                builder.guid);
    }

    public static final class Builder {
        private @JacksonXmlProperty String title;
        private @JacksonXmlProperty String description;
        private @JacksonXmlProperty String link;
        private @JacksonXmlProperty String author;
        private @JacksonXmlProperty String pubDate;
        private @JacksonXmlProperty String guid;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder title(@JacksonXmlProperty String val) {
            title = val;
            return this;
        }

        public Builder description(@JacksonXmlProperty String val) {
            description = val;
            return this;
        }

        public Builder link(@JacksonXmlProperty String val) {
            link = val;
            return this;
        }

        public Builder author(@JacksonXmlProperty String val) {
            author = val;
            return this;
        }

        public Builder pubDate(@JacksonXmlProperty String val) {
            pubDate = val;
            return this;
        }

        public Builder guid(@JacksonXmlProperty String val) {
            guid = val;
            return this;
        }

        public Item build() {
            return new Item(this);
        }
    }
}
