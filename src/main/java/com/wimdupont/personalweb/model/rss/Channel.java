package com.wimdupont.personalweb.model.rss;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "channel")
public record Channel(
        @JacksonXmlProperty
        String title,
        @JacksonXmlProperty
        String link,
        @JacksonXmlProperty
        String description,
        @JacksonXmlProperty
        String language,
        @JacksonXmlProperty
        String pubDate,
        @JacksonXmlElementWrapper(useWrapping = false)
        List<Item> item
) {


    private Channel(Builder builder) {
        this(builder.title,
                builder.link,
                builder.description,
                builder.language,
                builder.pubDate,
                builder.item);
    }

    public static final class Builder {
        private @JacksonXmlProperty String title;
        private @JacksonXmlProperty String link;
        private @JacksonXmlProperty String description;
        private @JacksonXmlProperty String language;
        private @JacksonXmlProperty String pubDate;
        private @JacksonXmlElementWrapper(useWrapping = false) List<Item> item;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder title(@JacksonXmlProperty String val) {
            title = val;
            return this;
        }

        public Builder link(@JacksonXmlProperty String val) {
            link = val;
            return this;
        }

        public Builder description(@JacksonXmlProperty String val) {
            description = val;
            return this;
        }

        public Builder language(@JacksonXmlProperty String val) {
            language = val;
            return this;
        }

        public Builder pubDate(@JacksonXmlProperty String val) {
            pubDate = val;
            return this;
        }

        public Builder item(@JacksonXmlElementWrapper(useWrapping = false) List<Item> val) {
            item = val;
            return this;
        }

        public Channel build() {
            return new Channel(this);
        }
    }
}
