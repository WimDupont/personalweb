package com.wimdupont.personalweb.model;

public enum Coin {
    HEADS, TAILS
}
