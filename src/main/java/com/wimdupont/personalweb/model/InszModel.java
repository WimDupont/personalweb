package com.wimdupont.personalweb.model;

import com.wimdupont.personalweb.model.validator.ValidInszModel;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDate;
import java.util.Random;

@ValidInszModel
public record InszModel(
        @NotNull
        LocalDate birthDate,
        @Min(value = 1)
        @Max(value = 998)
        @NotNull
        Integer birthCounter,
        @NotNull
        Gender gender
) {

    public InszModel(LocalDate birthDate, Integer birthCounter, Gender gender) {
        this.birthDate = generateBirthDateIfNull(birthDate);
        this.birthCounter = getRandomIfNull(birthCounter);
        this.gender = getRandomIfNull(gender);
    }

    private Gender getRandomIfNull(Gender gender) {
        return gender == null
                ? Gender.values()[(new Random().nextInt(Gender.values().length))]
                : gender;
    }

    private int getRandomIfNull(Integer value) {
        return value == null
                ? generateRandom(1, 998)
                : value;
    }

    private LocalDate generateBirthDateIfNull(LocalDate birthDate) {
        if (birthDate == null) {
            int minDay = (int) LocalDate.of(0, 1, 1).toEpochDay();
            int maxDay = (int) LocalDate.of(2999, 1, 1).toEpochDay();
            birthDate = LocalDate.ofEpochDay(generateRandom(minDay, maxDay));
        }
        return birthDate;
    }

    private int generateRandom(int min, int max) {
        return new Random().nextInt(max - min + 1) + min;
    }

}
