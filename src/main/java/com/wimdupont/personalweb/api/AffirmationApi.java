package com.wimdupont.personalweb.api;

import com.wimdupont.personalweb.api.dto.Affirmation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Component
public class AffirmationApi {

    private final String affirmationUrl;
    private final WebClient webClient;

    public AffirmationApi(@Value("${affirmation.url}") String affirmationUrl,
                          WebClient webClient) {
        this.affirmationUrl = affirmationUrl;
        this.webClient = webClient;
    }

    public Mono<Affirmation> getAffirmation() {
        return webClient.get()
                .uri(affirmationUrl)
                .retrieve()
                .bodyToMono(Affirmation.class)
                .timeout(Duration.ofSeconds(3))
                .onErrorReturn(new Affirmation("You're the best!"));
    }

}
