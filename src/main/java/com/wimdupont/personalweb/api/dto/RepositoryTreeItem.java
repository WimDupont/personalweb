package com.wimdupont.personalweb.api.dto;

public record RepositoryTreeItem(
        String path,
        String name
) {
}
