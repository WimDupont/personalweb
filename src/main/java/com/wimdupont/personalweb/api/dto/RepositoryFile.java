package com.wimdupont.personalweb.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public record RepositoryFile(

        @JsonProperty(value = "file_path")
        String path,
        @JsonProperty(value = "content_sha256")
        String contentSha256,
        @JsonProperty(value = "content")
        String contentBase64,
        @JsonProperty(value = "last_commit_id")
        String lastCommitId

) {
}
