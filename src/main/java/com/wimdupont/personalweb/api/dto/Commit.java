package com.wimdupont.personalweb.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public record Commit(
        @JsonProperty(value = "committed_date")
        Instant committedDate
) {
}
