package com.wimdupont.personalweb.api.dto;

public record Affirmation(
        String affirmation
) {
}
