package com.wimdupont.personalweb.api;

import com.wimdupont.personalweb.api.dto.Commit;
import com.wimdupont.personalweb.api.dto.RepositoryFile;
import com.wimdupont.personalweb.api.dto.RepositoryTreeItem;
import com.wimdupont.personalweb.model.AdocContentType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriUtils;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

@Component
public class GitApi {

    private final WebClient webClient;
    private final String repositoryUrl;
    private final String gitBranch;

    public GitApi(@Value("${git.repository.url}") String repositoryUrl,
                  @Value("${git.repository.branch}") String gitBranch,
                  WebClient webClient) {
        this.repositoryUrl = repositoryUrl;
        this.gitBranch = gitBranch;
        this.webClient = webClient;
    }

    public Optional<List<RepositoryTreeItem>> getRepositoryTree(AdocContentType adocContentType) {
        return webClient.get().uri(String.format("%s/tree?ref=%s&path=%s",
                        repositoryUrl,
                        gitBranch,
                        adocContentType.getPathPrefix()))
                .retrieve()
                .bodyToFlux(RepositoryTreeItem.class)
                .timeout(Duration.ofSeconds(5))
                .collectList().blockOptional();
    }

    public Optional<RepositoryFile> getRepositoryFile(String filePath) {
        return webClient.get().uri(URI.create(String.format("%s/files/%s?ref=%s",
                        repositoryUrl,
                        UriUtils.encode(filePath, StandardCharsets.UTF_8),
                        gitBranch)))
                .retrieve().bodyToMono(RepositoryFile.class)
                .timeout(Duration.ofSeconds(5))
                .blockOptional();
    }

    public Optional<Commit> getCommit(String commitId) {
        return webClient.get().uri(String.format("%s/commits/%s",
                        repositoryUrl,
                        commitId))
                .retrieve().bodyToMono(Commit.class)
                .timeout(Duration.ofSeconds(5))
                .blockOptional();
    }

}
