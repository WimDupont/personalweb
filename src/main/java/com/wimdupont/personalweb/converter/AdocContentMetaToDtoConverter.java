package com.wimdupont.personalweb.converter;

import com.wimdupont.personalweb.model.dao.projection.AdocContentMeta;
import com.wimdupont.personalweb.model.dto.AdocContentMetaDto;
import com.wimdupont.personalweb.model.dto.AdocContentMetaDto.Builder;
import com.wimdupont.personalweb.util.AdocContentUtil;
import org.springframework.stereotype.Component;

@Component
public class AdocContentMetaToDtoConverter {

    public AdocContentMetaDto convertForMetaData(AdocContentMeta adocContentMeta) {
        return Builder.newBuilder()
                .title(AdocContentUtil.toTitle(adocContentMeta))
                .dateTime(adocContentMeta.getCommittedDate().toLocalDate())
                .build();
    }
}
