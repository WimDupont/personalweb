package com.wimdupont.personalweb.converter;

import com.wimdupont.personalweb.model.dao.Book;
import com.wimdupont.personalweb.model.dto.BookDto;
import org.springframework.stereotype.Component;

@Component
public class BookToDtoConverter {

    public BookDto convert(Book entity) {
        return BookDto.Builder.newBuilder()
                .id(entity.getId())
                .title(entity.getTitle())
                .author(entity.getAuthor())
                .isbn(entity.getIsbn())
                .category(entity.getCategory())
                .series(entity.getSeries())
                .seriesNumber(entity.getSeriesNumber())
                .build();
    }
}
