package com.wimdupont.personalweb.converter;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.springframework.stereotype.Component;

@Component
public class AdocToHtmlStringConverter {

    private final Asciidoctor asciidoctor;
    private final Options options;

    public AdocToHtmlStringConverter(Asciidoctor asciidoctor,
                                     Options options) {
        this.asciidoctor = asciidoctor;
        this.options = options;
    }

    public String convert(String adocString) {
        return asciidoctor.convert(adocString, options);
    }
}
