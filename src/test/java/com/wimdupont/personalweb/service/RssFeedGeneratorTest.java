package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.model.dao.AdocContentMother;
import com.wimdupont.personalweb.model.dao.AdocContentMother.Defaults;
import com.wimdupont.personalweb.model.rss.RssFeed;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class RssFeedGeneratorTest {

    @Mock
    private AdocContentService adocContentService;
    @Mock
    private RssService rssService;
    @InjectMocks
    private RssFeedGenerator rssFeedGenerator;
    @Captor
    private ArgumentCaptor<RssFeed> rssFeedArgumentCaptor;

    @Test
    public void generate() {
        Mockito.when(adocContentService.findAllRSSData())
                .thenReturn(List.of(AdocContentMother.withDefaults().build()));

        rssFeedGenerator.generate();

        Mockito.verify(rssService, Mockito.times(1))
                .save(rssFeedArgumentCaptor.capture());
        var channel = rssFeedArgumentCaptor.getValue().channel();
        Assertions.assertEquals("2.0", rssFeedArgumentCaptor.getValue().version());
        Assertions.assertEquals("Wim Dupont", channel.title());
        Assertions.assertEquals("https://wimdupont.com", channel.link());
        Assertions.assertEquals("Updates from Wim Dupont", channel.description());
        Assertions.assertEquals("en-US", channel.language());
        Assertions.assertNotNull(channel.pubDate());
        Assertions.assertEquals(1, channel.item().size());
        Assertions.assertEquals(Defaults.PATH, channel.item().getFirst().title());
        Assertions.assertEquals("Wim Dupont", channel.item().getFirst().author());
        Assertions.assertEquals(String.format("https://wimdupont.com/blog/article/%s", Defaults.PATH), channel.item().getFirst().link());
        Assertions.assertEquals(Defaults.HTML_TEXT, channel.item().getFirst().description());
        Assertions.assertEquals(Defaults.COMMITTED_DATE.toString(), channel.item().getFirst().pubDate());
        Assertions.assertEquals(String.format("%s-%s", Defaults.PATH, Defaults.COMMITTED_DATE), channel.item().getFirst().guid());
    }

}
