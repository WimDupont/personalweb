package com.wimdupont.personalweb.service;


import com.wimdupont.personalweb.converter.BookToDtoConverter;
import com.wimdupont.personalweb.model.dao.Book;
import com.wimdupont.personalweb.model.dao.BookMother;
import com.wimdupont.personalweb.model.dto.BookDto;
import com.wimdupont.personalweb.repository.BookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {


    private List<Book> bookList;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private BookToDtoConverter bookToDtoConverter;
    @InjectMocks
    private BookService bookService;
    @Captor
    private ArgumentCaptor<Sort> sortArgumentCaptor;


    @BeforeEach
    public void setup() {
        bookList = List.of(
                BookMother.withDefaults(),
                BookMother.withDefaults(),
                BookMother.withDefaults()
        );
    }

    @Test
    public void findAllSortedByCategory() {
        Mockito.when(bookRepository.findAll(sortArgumentCaptor.capture()))
                .thenReturn(bookList);
        Mockito.when(bookToDtoConverter.convert(Mockito.any(Book.class)))
                .thenReturn(BookDto.Builder.newBuilder()
                                .id("1")
                                .category("Fun")
                                .build(),
                        BookDto.Builder.newBuilder()
                                .id("2")
                                .category("Fun")
                                .build(),
                        BookDto.Builder.newBuilder()
                                .id("3")
                                .category("Serious")
                                .build());

        var result = bookService.findAllSortedByCategory();

        Mockito.verify(bookToDtoConverter, Mockito.times(3))
                .convert(Mockito.any(Book.class));
        var sortList = sortArgumentCaptor.getValue().get().toList();
        Assertions.assertEquals(5, sortList.size());
        Assertions.assertEquals("category", sortList.get(0).getProperty());
        Assertions.assertEquals(Direction.ASC, sortList.get(0).getDirection());
        Assertions.assertEquals("author", sortList.get(1).getProperty());
        Assertions.assertEquals(Direction.ASC, sortList.get(1).getDirection());
        Assertions.assertEquals("series", sortList.get(2).getProperty());
        Assertions.assertEquals(Direction.ASC, sortList.get(2).getDirection());
        Assertions.assertEquals("seriesNumber", sortList.get(3).getProperty());
        Assertions.assertEquals(Direction.ASC, sortList.get(3).getDirection());
        Assertions.assertEquals("title", sortList.get(4).getProperty());
        Assertions.assertEquals(Direction.ASC, sortList.get(4).getDirection());
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(result.get("Fun").size(), 2);
        Assertions.assertEquals(result.get("Serious").size(), 1);
    }

}
