package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.api.dto.RepositoryFile;
import com.wimdupont.personalweb.api.dto.RepositoryFileMother;
import com.wimdupont.personalweb.model.dao.AdocContent;
import com.wimdupont.personalweb.model.dao.AdocContentMother;
import com.wimdupont.personalweb.model.dao.AdocContentMother.Defaults;
import com.wimdupont.personalweb.repository.AdocContentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class AdocContentServiceTest {

    private RepositoryFile repositoryFile;
    @Mock
    private AdocContentRepository adocContentRepository;
    @InjectMocks
    private AdocContentService adocContentService;

    @BeforeEach
    public void setup() {
        repositoryFile = RepositoryFileMother.withDefaults();
    }

    @Test
    public void removeNotFound() {
        Mockito.when(adocContentRepository.findAllByContentType(Defaults.ADOC_CONTENT_TYPE))
                .thenReturn(List.of(
                        AdocContentMother.withDefaults().path("path").build(),
                        AdocContentMother.withDefaults().path("someOtherPath").build()));

        adocContentService.removeWhenRemoteNotFound(Defaults.ADOC_CONTENT_TYPE, List.of(repositoryFile));

        Mockito.verify(adocContentRepository, Mockito.times(1))
                .delete(Mockito.any(AdocContent.class));
    }

}
