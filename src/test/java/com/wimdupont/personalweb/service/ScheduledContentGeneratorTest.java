package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.api.dto.Commit;
import com.wimdupont.personalweb.api.dto.CommitMother;
import com.wimdupont.personalweb.api.dto.RepositoryFile;
import com.wimdupont.personalweb.api.dto.RepositoryFileMother;
import com.wimdupont.personalweb.converter.AdocToHtmlStringConverter;
import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.model.dao.AdocContent;
import com.wimdupont.personalweb.model.dao.AdocContentMother;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class ScheduledContentGeneratorTest {

    private RepositoryFile repositoryFile;
    private Commit commit;
    @Mock
    private RssFeedGenerator rssFeedGenerator;
    @Mock
    private AdocContentService adocContentService;
    @Mock
    private RepositoryFileService repositoryFileService;
    @Mock
    private AdocToHtmlStringConverter adocToHtmlStringConverter;
    @InjectMocks
    private ScheduledContentGenerator scheduledContentGenerator;
    @Captor
    private ArgumentCaptor<AdocContent> adocContentArgumentCaptor;

    @BeforeEach
    public void setup() {
        repositoryFile = RepositoryFileMother.withDefaults();
        commit = CommitMother.withDefaults();
    }

    @Test
    public void generateWhenHashFoundShouldNotSaveEntity() {
        var guideToUpsert = AdocContentMother.withDefaults().contentSha256("contentSha256").build();
        var foundFiles = List.of(repositoryFile);
        Mockito.when(repositoryFileService.findRepositoryFiles(AdocContentType.GUIDE))
                .thenReturn(new ArrayList<>());
        Mockito.when(repositoryFileService.findRepositoryFiles(AdocContentType.BLOG_ARTICLE))
                .thenReturn(List.of(repositoryFile));
        Mockito.when(adocContentService.findByContentSha256(repositoryFile.contentSha256()))
                .thenReturn(Optional.of(guideToUpsert));

        scheduledContentGenerator.generate();

        Mockito.verify(adocContentService, Mockito.times(1))
                .removeWhenRemoteNotFound(AdocContentType.BLOG_ARTICLE, foundFiles);
        Mockito.verify(rssFeedGenerator, Mockito.times(1))
                .generate();
        Mockito.verify(adocContentService, Mockito.times(0))
                .save(adocContentArgumentCaptor.capture());
    }

    @Test
    public void generateWhenHashNotFoundShouldSaveEntity() {
        var foundFiles = List.of(repositoryFile);
        Mockito.when(repositoryFileService.findRepositoryFiles(AdocContentType.GUIDE))
                .thenReturn(new ArrayList<>());
        Mockito.when(repositoryFileService.findRepositoryFiles(AdocContentType.BLOG_ARTICLE))
                .thenReturn(List.of(repositoryFile));
        Mockito.when(adocContentService.findByContentSha256(repositoryFile.contentSha256()))
                .thenReturn(Optional.empty());
        Mockito.when(adocToHtmlStringConverter.convert(Mockito.anyString()))
                .thenReturn("content");
        Mockito.when(repositoryFileService.getCommit(repositoryFile.lastCommitId()))
                .thenReturn(Optional.of(commit));

        scheduledContentGenerator.generate();

        Mockito.verify(adocContentService, Mockito.times(1))
                .removeWhenRemoteNotFound(AdocContentType.BLOG_ARTICLE, foundFiles);
        Mockito.verify(rssFeedGenerator, Mockito.times(1))
                .generate();
        Mockito.verify(adocContentService, Mockito.times(1))
                .save(adocContentArgumentCaptor.capture());

        Assertions.assertEquals(repositoryFile.path(), adocContentArgumentCaptor.getValue().getPath());
        Assertions.assertEquals(repositoryFile.contentSha256(), adocContentArgumentCaptor.getValue().getContentSha256());
        Assertions.assertEquals("content", adocContentArgumentCaptor.getValue().getHtmlText());
        Assertions.assertEquals(AdocContentType.BLOG_ARTICLE, adocContentArgumentCaptor.getValue().getContentType());
        Assertions.assertEquals(LocalDateTime.ofInstant(commit.committedDate(), ZoneId.systemDefault()),
                adocContentArgumentCaptor.getValue().getCommittedDate());
    }
}
