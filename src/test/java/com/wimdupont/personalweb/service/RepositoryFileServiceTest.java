package com.wimdupont.personalweb.service;

import com.wimdupont.personalweb.api.GitApi;
import com.wimdupont.personalweb.api.dto.RepositoryFile;
import com.wimdupont.personalweb.api.dto.RepositoryFileMother;
import com.wimdupont.personalweb.api.dto.RepositoryTreeItem;
import com.wimdupont.personalweb.model.AdocContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class RepositoryFileServiceTest {

    private RepositoryFile repositoryFile;
    private List<RepositoryTreeItem> repositoryTreeItems;
    @Mock
    private GitApi gitApi;
    @InjectMocks
    private RepositoryFileService repositoryFileService;

    @BeforeEach
    public void setup() {
        repositoryTreeItems = List.of(new RepositoryTreeItem("path", "name"));
        repositoryFile = RepositoryFileMother.withDefaults();
    }

    @Test
    public void findRepositoryFiles() {
        Mockito.when(gitApi.getRepositoryTree(AdocContentType.GUIDE))
                .thenReturn(Optional.of(repositoryTreeItems));
        Mockito.when(gitApi.getRepositoryFile("path"))
                .thenReturn(Optional.of(repositoryFile));

        var result = repositoryFileService.findRepositoryFiles(AdocContentType.GUIDE);

        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(repositoryFile, result.getFirst());
    }

}
