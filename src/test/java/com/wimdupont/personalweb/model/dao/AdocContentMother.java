package com.wimdupont.personalweb.model.dao;

import com.wimdupont.personalweb.model.AdocContentType;

import java.time.LocalDateTime;

public class AdocContentMother {

    public static class Defaults {
        public static final String PATH = "path";
        public static final String HTML_TEXT = "htmlText";
        public static final String CONTENT_SHA_256 = "contentSha256";
        public static final LocalDateTime COMMITTED_DATE = LocalDateTime.of(2000, 1, 1, 0, 0);
        public static final AdocContentType ADOC_CONTENT_TYPE = AdocContentType.GUIDE;
    }

    public static AdocContent.Builder withDefaults() {
        return AdocContent.Builder.newBuilder()
                .path(Defaults.PATH)
                .htmlText(Defaults.HTML_TEXT)
                .contentSha256(Defaults.CONTENT_SHA_256)
                .committedDate(Defaults.COMMITTED_DATE)
                .contentType(Defaults.ADOC_CONTENT_TYPE);
    }

}
