package com.wimdupont.personalweb.model.dao;

import java.math.BigDecimal;

public class BookMother {

    public static class Defaults {
        public static final String ID = "id";
        public static final String TITLE = "title";
        public static final String AUTHOR = "author";
        public static final String ISBN = "isbn";
        public static final String CATEGORY = "category";
        public static final String SERIES = "series";
        public static final BigDecimal SERIES_NUMBER = BigDecimal.valueOf(2.5);
    }

    public static Book withDefaults() {
        var book = new Book();
        book.setId(Defaults.ID);
        book.setTitle(Defaults.TITLE);
        book.setAuthor(Defaults.AUTHOR);
        book.setIsbn(Defaults.ISBN);
        book.setCategory(Defaults.CATEGORY);
        book.setSeries(Defaults.SERIES);
        book.setSeriesNumber(Defaults.SERIES_NUMBER);
        return book;
    }
}
