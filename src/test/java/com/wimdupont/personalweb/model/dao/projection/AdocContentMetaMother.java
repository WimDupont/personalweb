package com.wimdupont.personalweb.model.dao.projection;

import com.wimdupont.personalweb.model.AdocContentType;

import java.time.LocalDateTime;

public class AdocContentMetaMother {

    public static class Defaults {
        public static final AdocContentType ADOC_CONTENT_TYPE = AdocContentType.GUIDE;
        public static final LocalDateTime COMMITTED_DATE = LocalDateTime.of(2000, 1, 1, 0, 0);
    }

    public static AdocContentMeta withDefaults(String path) {
        return new AdocContentMeta() {
            @Override
            public String getPath() {
                return path;
            }

            @Override
            public AdocContentType getContentType() {
                return Defaults.ADOC_CONTENT_TYPE;
            }

            @Override
            public LocalDateTime getCommittedDate() {
                return Defaults.COMMITTED_DATE;
            }
        };
    }

}
