package com.wimdupont.personalweb.api.dto;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class CommitMother {

    public static class Defaults {
        public static final Instant COMMITTED_DATE = LocalDateTime.now().toInstant(ZoneOffset.UTC);
    }

    public static Commit withDefaults() {
        return new Commit(Defaults.COMMITTED_DATE);
    }
}
