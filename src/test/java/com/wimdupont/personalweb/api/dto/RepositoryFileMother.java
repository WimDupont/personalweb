package com.wimdupont.personalweb.api.dto;

public class RepositoryFileMother {

    public static class Defaults {
        public static final String PATH = "path";
        public static final String CONTENT_SHA256 = "contentSha256";
        public static final String CONTENT_BASE64 = "Y29udGVudA==";
        public static final String LAST_COMMIT_ID = "lastCommitId";
    }

    public static RepositoryFile withDefaults() {
        return new RepositoryFile(
                Defaults.PATH,
                Defaults.CONTENT_SHA256,
                Defaults.CONTENT_BASE64,
                Defaults.LAST_COMMIT_ID);
    }
}
