package com.wimdupont.personalweb.converter;

import com.wimdupont.personalweb.model.dao.Book;
import com.wimdupont.personalweb.model.dao.BookMother;
import com.wimdupont.personalweb.model.dto.BookDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BookToDtoConverterTest {

    private BookToDtoConverter converter;
    private Book book;

    @BeforeEach
    void setup() {
        converter = new BookToDtoConverter();
        book = BookMother.withDefaults();
    }

    @Test
    void convert() {
        BookDto result = converter.convert(book);
        Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(book);
    }

}
