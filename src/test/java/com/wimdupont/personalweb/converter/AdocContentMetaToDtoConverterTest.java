package com.wimdupont.personalweb.converter;

import com.wimdupont.personalweb.model.AdocContentType;
import com.wimdupont.personalweb.model.dao.projection.AdocContentMetaMother;
import com.wimdupont.personalweb.model.dao.projection.AdocContentMetaMother.Defaults;
import com.wimdupont.personalweb.model.dto.AdocContentMetaDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AdocContentMetaToDtoConverterTest {

    private AdocContentMetaToDtoConverter converter;

    @BeforeEach
    void setup() {
        converter = new AdocContentMetaToDtoConverter();
    }

    @Test
    void convertWithAdocExtensionShouldRemoveExtension() {
        AdocContentMetaDto result = converter.convertForMetaData(
                AdocContentMetaMother.withDefaults("guide.adoc"));

        Assertions.assertEquals("guide", result.title());
        Assertions.assertEquals(Defaults.COMMITTED_DATE.toLocalDate(), result.date());
    }

    @Test
    void convertWithOtherExtensionShouldNotRemoveExtension() {
        AdocContentMetaDto result = converter.convertForMetaData(
                AdocContentMetaMother.withDefaults("guide.md"));

        Assertions.assertEquals("guide.md", result.title());
        Assertions.assertEquals(Defaults.COMMITTED_DATE.toLocalDate(), result.date());
    }

    @Test
    void convertWithoutExtensionRemainTheSame() {
        AdocContentMetaDto result = converter.convertForMetaData(
                AdocContentMetaMother.withDefaults("guide"));

        Assertions.assertEquals("guide", result.title());
        Assertions.assertEquals(Defaults.COMMITTED_DATE.toLocalDate(), result.date());
    }

    @Test
    void convertWithTypePrefixShouldRemove() {
        AdocContentMetaDto result = converter.convertForMetaData(
                AdocContentMetaMother.withDefaults(AdocContentType.GUIDE.getPathPrefix() + "/guide"));

        Assertions.assertEquals("guide", result.title());
        Assertions.assertEquals(Defaults.COMMITTED_DATE.toLocalDate(), result.date());
    }

}
